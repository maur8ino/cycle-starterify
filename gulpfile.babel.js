import gulp from 'gulp';
import babelify from 'babelify';
import hotModuleReloading from 'browserify-hmr';
import budo from 'budo';
import eslint from 'gulp-eslint';

const entry = './index.js';
const srcLint = ['src/**/*.js', 'test/**/*.js'];

gulp.task('watch', ['lint'], function(cb) {
  // linting every time
  gulp.watch(srcLint, ['lint']);

  //dev server
  budo(entry, {
    serve: 'bundle.js',            // end point for our <script> tag
    stream: process.stdout,        // pretty-print requests
    live: '**/*.{html,css}',       // live reload & CSS injection
    dir: 'dist',                   // directory to serve
    open: true,                    // whether to open the browser
    browserify: {
      transform: babelify,         //browserify transforms
      plugin: hotModuleReloading   //browserify hot module reloading
    }
  }).on('exit', cb);
});

gulp.task('lint', () => {
  gulp.src(srcLint)
  .pipe(eslint())
  .pipe(eslint.format());
});
